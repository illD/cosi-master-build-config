/**
 * Compile LESS files to CSS
 * https://github.com/gruntjs/grunt-contrib-less
 */
 module.exports = {
 	modules: {
 		files: {
 			"css/modules.css": "modules/**/*.less"
 		}
 	},
 	bootstrapDev: {
 		options: {
 			paths: ["node_modules/bootstrap/less"],
 			compress: false,
	 		modifyVars: {
				'iconfontpath':  '"../node_modules/bootstrap/fonts/"'
	 		}
 		},
 		files: {
 			"css/bootstrap.css": "css/bootstrap.less"
 		}
 	},
 	bootstrapBuild: {
 		options: {
 			paths: ["node_modules/bootstrap/less"],
 			compress: true,
	 		modifyVars: {
				'iconfontpath':  '"../fonts/"'
	 		}
 		},
 		files: {
 			"css/bootstrap.css": "css/bootstrap.less"
 		}
 	}
 };
